module LoginMacros

  def log_in(user)
    visit root_path
    click_link 'Login'
    fill_in 'Name', with: user.name
    fill_in 'Password', with: user.password 
    click_button 'Sign in'
  end

end