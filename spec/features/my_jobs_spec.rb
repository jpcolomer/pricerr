require'spec_helper'

feature 'Job management' do
  
  before(:each) do
    load "#{Rails.root}/db/seeds.rb" 
  end  
  scenario 'adds a new job' do
    user = create(:user)

    log_in(user)

    fill_in 'I will', with: 'do sth super cool'
    fill_in 'for' , with: '20'
    click_button 'Continue'

    expect(find('#job_title').value).to eq 'do sth super cool'
    expect(find('#job_price').value).to eq '20'
    

    fill_in 'Description', with: 'blabla'
    select 'Events', from: 'Category'
    select 'Alabama', from: 'Location'

    fill_in 'Max Days to Deliver', with: 20

    expect{
      click_button 'Create Job'
    }.to change(Job, :count).by(1)

    expect(current_path).to eq root_path
    expect(page).to have_content 'Job was succesfully created.'

  end
end

feature 'My Seller Jobs' do
  before(:each) do
    load "#{Rails.root}/db/seeds.rb" 
    user = create(:user)
    log_in(user)
    @jobs = create_list(:job, 2, user: user)
  end

  scenario 'watch Active Jobs' do
    active_jobs = @jobs.map {|job| create(:active_job, job: job)}
    click_link 'My account'
    click_link 'My Jobs'
    expect(page).to have_content active_jobs.size
    click_link 'Active'

    within '#active_jobs' do
      expect(page).to have_link 'Active'
      expect(find_link('Active')[:href]).to eq my_jobs_path(status: :active)
      expect(page).to have_content active_jobs.size
    end

    expect(current_path).to eq my_jobs_path
    expect(page).to have_content 'Active Jobs'
    
    active_jobs.each do |job|
      within "#purchase_job_#{job.id}" do
        expect(page).to have_content job.title
        expect(page).to have_content job.buyer
        expect(page).to have_content job.deliver_date
      end
    end
  end

  scenario 'watch Finished Jobs' do
    jobs = @jobs.map {|job| create(:finished_job, job: job)}
    click_link 'My account'
    click_link 'My Jobs'
    expect(page).to have_content jobs.size
    click_link 'Finished'

    within '#finished_jobs' do
      expect(page).to have_link 'Finished'
      expect(find_link('Finished')[:href]).to eq my_jobs_path(status: :finished)
      expect(page).to have_content jobs.size
    end

    expect(current_path).to eq my_jobs_path
    expect(page).to have_content 'Finished Jobs'
    
    within 'thead' do
      expect(page).to have_content 'Title'
      expect(page).to have_content 'Customer'
      expect(page).to have_content 'Delivery Date'
      expect(page).to have_content 'Review'
    end


    jobs.each do |job|
      within "#purchase_job_#{job.id}" do
        expect(page).to have_content job.title
        expect(page).to have_content job.buyer
        expect(page).to have_content job.deliver_date
        expect(page).to have_content job.review
      end
    end
  end


  scenario 'watch Unreviewed Jobs' do
    jobs = @jobs.map {|job| create(:unreviewed_job, job: job)}
    click_link 'My account'
    click_link 'My Jobs'
    expect(page).to have_content jobs.size
    click_link 'In Review'

    within '#unreviewed_jobs' do
      expect(page).to have_link 'In Review'
      expect(find_link('In Review')[:href]).to eq my_jobs_path(status: :unreviewed)
      expect(page).to have_content jobs.size
    end
    
    expect(current_path).to eq my_jobs_path
    expect(page).to have_content 'In Review Jobs'
    
    jobs.each do |job|
      within "#purchase_job_#{job.id}" do
        expect(page).to have_content job.title
        expect(page).to have_content job.buyer
        expect(page).to have_content job.deliver_date
      end
    end
  end
end

feature 'My Buyer Jobs' do
  before(:each) do
    load "#{Rails.root}/db/seeds.rb" 
    @user = create(:user)
    log_in(@user)
  end

  scenario 'go to active jobs' do
    purchase_jobs = create_list(:active_job, 2, user: @user)
    click_link 'My account'
    click_link 'My Purchases'
    expect(current_path).to eq purchase_jobs_path
    expect(find_link('Active Jobs')[:href]).to eq purchase_jobs_path(status: :active)
    click_link 'Active Jobs'
    expect(current_path).to eq purchase_jobs_path

    within '.table' do
      expect(page).to have_content 'created on'
      expect(page).to have_content 'delivery on'
      expect(page).to have_content 'total'
    end

    purchase_jobs.each do |job|
       within "#purchase_job_#{job.id}" do
        expect(page).to have_content job.created_at.to_date
        expect(page).to have_content job.deliver_date
        expect(page).to have_content job.title
        expect(page).to have_content job.price
      end     
    end
  end

  scenario 'go to finished jobs' do
    purchase_jobs = create_list(:finished_job, 2, user: @user)
    click_link 'My account'
    click_link 'My Purchases'
    expect(current_path).to eq purchase_jobs_path
    expect(find_link('Finished Jobs')[:href]).to eq purchase_jobs_path(status: :finished)
    click_link 'Finished Jobs'
    expect(current_path).to eq purchase_jobs_path

    within '.table' do
      expect(page).to have_content 'created on'
      expect(page).to have_content 'delivery on'
      expect(page).to have_content 'total'
    end

    purchase_jobs.each do |job|
       within "#purchase_job_#{job.id}" do
        expect(page).to have_content job.created_at.to_date
        expect(page).to have_content job.deliver_date
        expect(page).to have_content job.title
        expect(page).to have_content job.price
      end     
    end
  end


  scenario 'go to unreviewed jobs' do
    purchase_jobs = create_list(:unreviewed_job, 2, user: @user)
    click_link 'My account'
    click_link 'My Purchases'
    expect(current_path).to eq purchase_jobs_path
    expect(find_link('Pending Review Jobs')[:href]).to eq purchase_jobs_path(status: :unreviewed)
    click_link 'Pending Review Jobs'
    expect(current_path).to eq purchase_jobs_path

    within '.table' do
      expect(page).to have_content 'created on'
      expect(page).to have_content 'delivery on'
      expect(page).to have_content 'total'
    end

    purchase_jobs.each do |job|
       within "#purchase_job_#{job.id}" do
        expect(page).to have_content job.created_at.to_date
        expect(page).to have_content job.deliver_date
        expect(page).to have_content job.title
        expect(page).to have_content job.price
      end     
    end
  end


end