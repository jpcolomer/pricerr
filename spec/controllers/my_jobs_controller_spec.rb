require 'spec_helper'

describe MyJobsController do

  describe 'GET #index' do
    before(:each) do
      load "#{Rails.root}/db/seeds.rb" 
    end  


    context 'with no status param and authentication' do
      it 'populates an array of active_jobs' do
        user = create(:user)
        sign_in user
        job = create(:job, user: user)

        purchase_job = create(:purchase_job, job: job)
        get :index
        expect(assigns(:jobs)).to match_array [purchase_job]
      end
    end

    ['active', 'finished', 'unreviewed'].each do |job_status|
      describe "for #{job_status} jobs" do

        context "with authentication" do
          before(:each) do
            @user = create(:user)
            sign_in @user
            @job = create(:job, user: @user)
          end

          it 'populates an array of jobs' do
            job = create("#{job_status}_job".to_sym, job: @job)
            create("#{job_status}_job".to_sym)
            get :index, status: job_status
            expect(assigns(:jobs)).to match_array [job]
          end

          it 'renders the :index view' do
            get :index, status: job_status
            expect(response).to render_template :index 
          end

        end

        context "no authentication" do
          it 'requires login' do
            get :index, status: job_status
            expect(response).to redirect_to new_user_session_url
          end          
        end

      end
    end

  end
end
