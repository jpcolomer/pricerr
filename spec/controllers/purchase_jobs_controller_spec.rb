require 'spec_helper'

describe PurchaseJobsController do

  describe 'GET #index' do
    before(:each) do
      load "#{Rails.root}/db/seeds.rb" 
    end  


    context 'with authentication' do
      it 'populates an array of purchase_jobs' do
        user = create(:user)
        sign_in user
        purchase_job = create(:purchase_job, user: user)
        get :index
        expect(assigns(:purchase_jobs)).to match_array [purchase_job]
      end

      it 'populates an array of active_jobs' do
        user = create(:user)
        sign_in user
        purchase_job = create(:active_job, user: user)
        create(:unreviewed_job, user: user)
        get :index, status: :active
        expect(assigns(:purchase_jobs)).to match_array [purchase_job]
      end

      it 'populates an array of finished_jobs' do
        user = create(:user)
        sign_in user
        purchase_job = create(:finished_job, user: user)
        create(:unreviewed_job, user: user)
        get :index, status: :finished
        expect(assigns(:purchase_jobs)).to match_array [purchase_job]
      end

      it 'populates an array of unreviewed_jobs' do
        user = create(:user)
        sign_in user
        purchase_job = create(:unreviewed_job, user: user)
        create(:active_job, user: user)
        get :index, status: :unreviewed
        expect(assigns(:purchase_jobs)).to match_array [purchase_job]
      end

    end

    context 'without authentication' do
      it 'redirects to login' do
        get :index
        expect(response).to redirect_to new_user_session_url
      end      
    end
  end

end
