require 'spec_helper'

describe JobsController do
  before(:each) do
    load "#{Rails.root}/db/seeds.rb" 
  end

  describe 'GET #index' do

    it 'populates an array of jobs' do
      jobs = create_list(:job,2)
      get :index
      expect(assigns(:jobs)).to match_array jobs
    end

    describe 'populates an array of jobs' do 

      it 'for specific category' do
        category = Category.first
        jobs = create_list(:job, 2, category: category)
        create_list(:job, 2, category: Category.last)
        get :index, category: category.name
        expect(assigns(:jobs)).to match_array jobs
      end

      it 'for specific location' do
        location = create(:location)
        jobs = create_list(:job, 2, location: location)
        create_list(:job, 2)
        get :index, location: location.name 
        expect(assigns(:jobs)).to match_array jobs
      end

      it 'for specific user' do
        user = create(:user)
        jobs = create_list(:job, 2, user: user)
        create_list(:job, 2)
        get :index, user_id: user.id
        expect(assigns(:jobs)).to match_array jobs
      end


      it 'for specific tag' do
        jobs = create_list(:job, 2, tag_list: 'programming')
        create_list(:job, 2)
        get :index, tag: 'programming'
        expect(assigns(:jobs)).to match_array jobs
      end

      it 'by title' do
        jobs = create_list(:job, 2, title: 'blablabla')
        create_list(:job, 2)
        get :index, q: 'blablabla'
        expect(assigns(:jobs)).to match_array jobs    
      end


      it 'by description' do
        jobs = create_list(:job, 2, description: 'blablabla')
        create_list(:job, 2)
        get :index, q: 'blablabla'
        expect(assigns(:jobs)).to match_array jobs    
      end
    end
  end

  describe 'GET #show' do
    it 'assigns the requested job to @job' do
      job = create(:job)
      create_list(:job, 5)
      get :show, id: job.id
      expect(assigns(:job)).to eq job
    end

    it 'renders the :show template' do
      job = create(:job)
      get :show, id: job.id
      expect(response).to render_template :show
    end
  end

  describe 'GET #new' do
    context "with credentiales" do
      before(:each) do
        @user = create(:user)
        sign_in @user
      end

      it 'assigns new job to @job' do
        get :new
        expect(assigns(:job)).to be_a_new(Job)
      end

      it 'renders the :new template' do
        get :new
        expect(response).to render_template :new
      end
    end

    context 'without credentials' do
      it 'redirects to log in page' do
        get :new
        expect(response).to redirect_to new_user_session_url
      end
    end
  end


  describe 'POST #create' do
    context "with credentiales" do
      before(:each) do
        @user = create(:user)
        sign_in @user
      end
      context "and valid attributes" do

        it 'saves the new message to the db' do
          expect{post :create, job: attributes_for(:job)}.to change(Job, :count).by(1)
        end

        it 'redirects to home path' do
          post :create, job: attributes_for(:job)
          expect(response).to redirect_to root_url
        end
      end

      context 'and invalid attributes' do
        it 'renders new action' do
          post :create, job: attributes_for(:invalid_job)
          expect(response).to render_template :new
        end
      end
    end

    context 'without credentials' do
      it 'redirects to log in page' do
        post :create, job: attributes_for(:job)
        expect(response).to redirect_to new_user_session_url
      end
    end
  end



end