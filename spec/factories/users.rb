require 'faker'

FactoryGirl.define do
  factory :user do
    sequence(:name) { |n| "#{Faker::Internet.user_name}_#{n}" }
    sequence(:email) { Faker::Internet.email }
    password '12345678'
  end
end