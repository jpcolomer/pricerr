require 'faker'

FactoryGirl.define do
  factory :category do
    sequence(:name) {|n| "Internet_#{n}"}
  end
end