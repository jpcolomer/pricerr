# Read about factories at https://github.com/thoughtbot/factory_girl
require 'faker'


FactoryGirl.define do
  factory :purchase_job do
    active true
    review nil
    user
    job


    factory :active_job do
      active true
    end

    factory :finished_job do
      active false
      review { Faker::Lorem.sentence(20) }
    end

    factory :unreviewed_job do
      active false
    end
  end

end
