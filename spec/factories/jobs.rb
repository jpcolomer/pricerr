# Read about factories at https://github.com/thoughtbot/factory_girl
require 'faker'

FactoryGirl.define do
  factory :job do

    user
    category_id {Category.order("RANDOM()").first.id}
    location
    description { "#{Faker::Lorem.sentence(20)}_#{rand(0..1000)}" }
    title { "#{Faker::Lorem.sentence(5)}_#{rand(0..1000)}" }
    price {rand(0..100)}
    days {rand(1..20)}

    factory :invalid_job do
      description nil
    end

  end


end
