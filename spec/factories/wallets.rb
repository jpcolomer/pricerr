require 'faker'

FactoryGirl.define do
  factory :wallet do
    user
    balance {rand(10..40)}
  end
end