require 'faker'

FactoryGirl.define do
  factory :request do
    description {Faker::Lorem.sentence(20)}
    user
    category_id {Category.order("RANDOM()").first.id}

    factory :approved_request do
      approved true
    end

    factory :nonapproved_request do
      approved false
    end

    factory :pending_approved_request do
      approved nil
    end
  end
end