# Read about factories at https://github.com/thoughtbot/factory_girl
require 'faker'

FactoryGirl.define do
  factory :extra_job do
    job
    description { Faker::Lorem.sentence(20) }
    price {rand(0..100)}
  end

end
