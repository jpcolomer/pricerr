require 'spec_helper'

describe User do

  it {should have_many(:purchase_jobs)}

  it 'is valid' do
    expect(build(:user)).to be_valid
  end

  it {should validate_presence_of(:email)}

  it 'is invalid without email' do
    expect(build(:user, email: nil)).to have(1).errors_on(:email)
  end

  it 'is invalid with invalid email' do
    expect(build(:user, email: '13254987@')).to have(1).errors_on(:email)
  end

  it 'is invalid with duplicate email' do
    user1 = create(:user)
    expect(build(:user, email: user1.email)).to have(1).errors_on(:email)
  end

  it 'is invalid with duplicate name' do
    user1 = create(:user)
    expect(build(:user, name: user1.name)).to have(1).errors_on(:name)
  end

  it {should validate_presence_of(:name)}
  it {should validate_uniqueness_of(:name)}

  describe 'associations' do
    it {should have_many(:jobs)}
    it {should have_many(:requests)}
    it {should have_one(:wallet)}
  end


  it 'creates wallet when create' do
    user = create(:user)
    expect(user.wallet).to be_an_instance_of(Wallet)
  end

  describe '.find_by_name_like' do 

    before do
      @juanitos = []
      4.times {|i| @juanitos << create(:user, name: "juanito#{i}")}
      4.times {|i| create(:user, name: "luchito#{i}")}
    end

    it 'returns an array that match' do
      expect(User.find_by_name_like('juanito')).to match_array @juanitos
    end

    it 'not include a one that not match' do
      smith = create(:user, name: 'smith')
      expect(User.find_by_name_like('juanito')).to_not include smith
    end


  end


  ['active', 'finished', 'unreviewed'].each do |job_status|
    it {should respond_to("#{job_status}_jobs")}
    describe "##{job_status}_jobs" do
      before(:each) do
        load "#{Rails.root}/db/seeds.rb" 
      end 

      it "returns an array of user #{job_status}" do
        user = create(:user)
        job = create(:job, user: user)
        purchase_jobs = create_list("#{job_status}_job".to_sym, 2, job: job)
        create_list("#{job_status}_job".to_sym, 3)

        expect(user.send("#{job_status}_jobs")).to match_array purchase_jobs
      end

    end
  end





end
