require 'spec_helper'

describe Wallet do

  it {should belong_to(:user)}

  it 'is invalid without user id' do
    expect(build(:wallet, user: nil)).to have(1).errors_on(:user_id)
  end

  it 'does not allow two wallets per user' do
    user = create(:user)
    expect(build(:wallet, user: user)).to have(1).errors_on(:user_id)
  end

  it 'does not allow negative balance' do
    expect(build(:wallet, balance: -1)).to have(1).errors_on(:balance)
  end
end
