require 'spec_helper'

describe PurchaseJob do
  
  it {should belong_to(:job)}
  it {should belong_to(:user)}

  it {should validate_presence_of(:user)}
  it {should validate_presence_of(:job)}


  describe '#title' do
    before(:each) do
      load "#{Rails.root}/db/seeds.rb" 
    end  

    it "delegates title method to job" do
      purchase_job = build_stubbed(:purchase_job)
      expect(purchase_job).to respond_to(:title)
      expect(purchase_job.title).to eq purchase_job.job.title
    end

  end

  describe '#days' do
    before(:each) do
      load "#{Rails.root}/db/seeds.rb" 
    end  

    it "delegates days method to job" do
      purchase_job = build_stubbed(:purchase_job)
      expect(purchase_job).to respond_to(:days)
      expect(purchase_job.days).to eq purchase_job.job.days
    end

  end


  describe '#price' do
    before(:each) do
      load "#{Rails.root}/db/seeds.rb" 
    end  

    it "delegates price method to job" do
      purchase_job = build_stubbed(:purchase_job)
      expect(purchase_job).to respond_to(:price)
      expect(purchase_job.days).to eq purchase_job.job.days
    end

  end



  describe '#deliver_date' do
    before(:each) do
      load "#{Rails.root}/db/seeds.rb" 
    end  

    it 'responds to deliver_date' do
      expect(build(:purchase_job)).to respond_to(:deliver_date)
    end

    it 'returns 2 days from now' do
      job = build_stubbed(:job, days: 2)
      expect(build_stubbed(:purchase_job, job: job).deliver_date).to eq 2.days.from_now.to_date
    end

    it 'returns 4 days from now' do
      job = build_stubbed(:job, days: 4)
      expect(build_stubbed(:purchase_job, job: job).deliver_date).to eq 4.days.from_now.to_date
    end
  end


  describe '#buyer' do
    before(:each) do
      load "#{Rails.root}/db/seeds.rb" 
    end

    it 'returns same user' do
      user = build_stubbed(:user)
      job = build(:purchase_job, user: user)
      expect(job.buyer).to eq user
    end

  end

  describe 'scopes' do
    before(:each) do
      load "#{Rails.root}/db/seeds.rb" 
    end

    describe '.active' do
      it 'returns a matching array' do
        active_jobs = create_list(:active_job, 2)
        create_list(:finished_job, 1)
        expect(PurchaseJob.active).to match_array active_jobs
      end

      it 'returns empty array for non matching' do
        create_list(:finished_job, 5)
        expect(PurchaseJob.active).to match_array []
      end
    end

    describe '.unreviewed' do
      before(:each) do
        create_list(:purchase_job, 2)
        create_list(:finished_job, 1)
      end
      it 'returns a matching array' do
        unreviewed = create_list(:unreviewed_job, 2)
        expect(PurchaseJob.unreviewed).to match_array unreviewed
      end

      it { expect(PurchaseJob.unreviewed).to match_array [] }
    end

    describe '.finished' do
      before(:each) do
        create_list(:purchase_job, 2)
        create_list(:unreviewed_job, 1)
      end

      it 'returns a matching array' do
        finished = create_list(:finished_job, 2)
        expect(PurchaseJob.finished).to match_array finished
      end

      it { expect(PurchaseJob.finished).to match_array [] }

    end
  end

end
