require 'spec_helper'

describe Job do

  before(:each) do
    load "#{Rails.root}/db/seeds.rb" 
  end

  it {should validate_presence_of(:user)}
  it {should validate_presence_of(:category)}
  it {should validate_presence_of(:description)}
  it {should validate_presence_of(:title)}
  it {should validate_presence_of(:price)}
  it {should validate_presence_of(:days)}

  it {should belong_to(:user)}
  it {should belong_to(:location)}
  it {should belong_to(:category)}
  it {should have_many(:images)}
  it {should have_many(:extra_jobs)}
  it {should have_many(:purchase_jobs)}


  it { expect(build(:job)).to be_valid }
  it { expect(build(:job, description: nil)).to have(1).errors_on(:description) }
  it { expect(build(:job, title: nil)).to have(1).errors_on(:title) }

  describe 'accepts nested attributes' do

    describe 'for extrajobs' do
      context "with valid attributes" do
        it{ expect{create(:job, extra_jobs_attributes: {0 => attributes_for(:extra_job)})}.to change(ExtraJob, :count).by(1) }
      end
      context "with invalid attributes" do
        it { expect{create(:job, extra_jobs_attributes: {0 => attributes_for(:extra_job, description: '')})}.to_not change(ExtraJob, :count)}
        it { expect{create(:job, extra_jobs_attributes: {0 => attributes_for(:extra_job, price: '')})}.to_not change(ExtraJob, :count)}
      end
    end
  end

  describe '#full_title' do
    it 'returns a string that match' do
      job = build(:job)
      expect(job.full_title).to eq "I will #{job.title} for #{ActionController::Base.helpers.number_to_currency(job.price)}"
    end
  end


  describe '.find_jobs_from_class' do

    {category: Category, location: Location}.each do |symbol, klass|
      it "return an array that match for class #{klass.name}" do
        type = klass.first
        jobs = create_list(:job, 10, "#{klass.name.downcase}_id".to_sym => type.id)
        create_list(:job, 10, "#{klass.name.downcase}_id".to_sym => klass.last.id)
        expect(Job.find_jobs_from_class(symbol, type.name)).to match_array jobs
      end
    end
  end

end
