require 'spec_helper'

describe Request do

  it {should belong_to(:user)}
  it {should belong_to(:category)}

  context "use of DB" do
    before(:each) do
      load "#{Rails.root}/db/seeds.rb" 
    end    


    it { expect(build(:request)).to be_valid}
    it {expect(build(:request, description: nil)).to have(1).errors_on(:description)}

    it 'returns an array of approved requests' do
      request = create_list(:approved_request,2)
      expect(Request.approved).to match_array request
    end

    it 'returns an array of non approved requests' do
      request = create_list(:nonapproved_request,2)
      expect(Request.non_approved).to match_array request
    end

    it 'returns an array of pending approval requests' do
      request = create_list(:request, 2)
      expect(Request.pending_approval).to match_array request
    end

  end


end
