class UsersController < ApplicationController

  def index
    @users = User.find_by_name_like params[:name]
    @users = @users.map{|user| user.name}
    render json: @users
  end

  def show
    @user = User.includes(:jobs).find(params[:id])
    @jobs = @user.jobs.recent
    @avatar = @user.avatar_url(:thumb)
  end
end
