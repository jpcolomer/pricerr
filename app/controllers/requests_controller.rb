class RequestsController < ApplicationController
  before_filter :authenticate_user!, except: :index

  def index
    @requests, @title = get_request_from_params
  end

  def show
    @user = User.find(params[:user_id])
    @request = Request.find(params[:id])
  end

  def create
    @user = User.find(params[:user_id])
    @request = @user.requests.new(params[:request])
    if @request.save
      redirect_to user_request_path(@user, @request), notice: 'Request created, please wait for approval'
    else
      redirect_to root_path
    end
  end

  private
  def get_request_from_params
    requests, title = params.key?(:category_id) ? [Request.where(category_id: params[:category_id]), " in #{Category.find(params[:category_id])}"] : [Request, '']
    requests = requests.approved.recent
    title = "All Requests#{title}"
    [requests, title]
  end

end
