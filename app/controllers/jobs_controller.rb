class JobsController < ApplicationController
  before_filter :authenticate_user!, except: [:index, :show]

  def index
    respond_to do |format|
      format.html do
        @jobs, @jobs_filter_title = Job.get_from_params(params)
        @jobs = @jobs.recent.paginate(page: params[:page])
      end

      format.json do
        @jobs = Job.find_by_title_or_description(params[:q]).recent.limit(5)
        render json: @jobs.to_json(only: [:title, :price, :id, :description])
      end
    end
  end


  def show
    @job = Job.includes(:images, :extra_jobs).find(params[:id])
    @tags = @job.tags
    @images = @job.images.recent
    @extra_jobs = @job.extra_jobs.recent
    unless @images.empty?
      @first_image = @images.first
      @other_images = @images[1..-1]
    end
  end

  def new
    @title = params[:title] || ''
    @price = params[:price] || ''
    @job = current_user.jobs.new
    4.times { @job.images.build }
    10.times { @job.extra_jobs.build }
  end

  def create
    @job = current_user.jobs.new(params[:job])
    if @job.save
      redirect_to(root_path, notice: 'Job was succesfully created.')
    else
      (4 - @job.images.size).times{ @job.images.build }
      render action: 'new'
    end
  end
end
