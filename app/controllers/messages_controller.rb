class MessagesController < ApplicationController
  before_filter :authenticate_user!

  layout 'conversations'

  def index
    conversation = Conversation.find(params[:conversation_id])
    @receipts = conversation.receipts_for current_user
    @active = "#{@receipts.first.mailbox_type}_active"
  end

  def new
  end

  def show
  end

  def destroy
  end

  def create
    recipient = User.find_by_name(params[:message][:recipient])
    if current_user.send_message(recipient, params[:message][:body], params[:message][:subject])
      redirect_to :root, notice: 'Message sent'
    else
      render action: 'new'
    end
  end

end
