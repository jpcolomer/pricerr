class HomeController < ApplicationController
  def index
    @jobs = Job.includes(:category)
    @jobs = Category.includes(:jobs).find_by_name(params[:category]).jobs if params[:category]
    @jobs = @jobs.recent.paginate(page: params[:page])
  end
end
