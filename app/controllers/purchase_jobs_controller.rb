class PurchaseJobsController < ApplicationController
  before_filter :authenticate_user!
  
  def index
    if ['active', 'finished', 'unreviewed'].include?(params[:status])
      @purchase_jobs = current_user.purchase_jobs.includes(:job).send(params[:status])
      @status = params[:status]
    else
      @purchase_jobs = current_user.purchase_jobs.includes(:job).active
      @status = 'active'
    end
  end

end