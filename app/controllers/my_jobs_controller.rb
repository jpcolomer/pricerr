class MyJobsController < ApplicationController
  before_filter :authenticate_user!
  def index
    if ['active', 'finished', 'unreviewed'].include?(params[:status])
      @jobs = current_user.send("#{params[:status]}_jobs")
      @title =  (params[:status] == 'unreviewed' ? 'In Review' : "#{params[:status].camelize}") + " Jobs"
    else
      @jobs = current_user.active_jobs
      @title = 'Active Jobs'
    end
  end
end
