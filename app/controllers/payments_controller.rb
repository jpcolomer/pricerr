class PaymentsController < ApplicationController

  def show
    @user = User.find(params[:user_id])
    redirect_to user_payment_path(current_user) unless same_user?(@user)
  end


  private

  def same_user?(user)
    current_user == user
  end
end
