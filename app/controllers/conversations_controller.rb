class ConversationsController < ApplicationController
	before_filter :authenticate_user!

	def index
    if params.key?(:mailbox) && ['inbox', 'sentbox', 'trash'].include?(params[:mailbox])
      @conversations = current_user.mailbox.send(params[:mailbox])
      @active = "#{params[:mailbox]}_active"
    else
      @conversations = current_user.mailbox.inbox
      @active = :inbox_active
    end
	end
end
