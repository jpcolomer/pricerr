# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$(document).ready ->
  $('#socialButtons').share
    networks: ['email','pinterest','tumblr','googleplus','facebook','twitter','linkedin','stumbleupon']
    theme: 'square'
    urlToShare: 'http://www.pricerr.com'
    title: $('.show-header').text().match(/: I will.+/i)[0].replace(/^: /,'')


window.goToJob = (id) ->
    window.location.replace("/jobs/#{id}")

$(".typeahead-job").typeahead
  highlighter: (item) ->
    "<div onclick='goToJob(#{item.job_id})'>I will #{item} for $#{item.price}</div>"

  matcher: (item) ->
    regex = new RegExp( '(' + this.query + ')', 'gi' )
    regex.test(item) || regex.test(item.description)


  source: (query, process) ->
    $.getJSON "/jobs.json", {q: query}, (json) ->
      process(for job in json
        item = new String "#{job.title}"
        item.price = job.price
        item.job_id = job.id
        item.description = job.description
        item)