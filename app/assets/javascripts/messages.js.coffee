$(".typeahead-user").typeahead
  source: (query, process) ->
    $.getJSON "/users", {name: query}, (json) ->
      process(json)