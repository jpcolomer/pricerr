module PurchaseJobsHelper
  def get_btn_class(purchase_job, status)
    css = 'btn btn-primary'
    css <<  ' disabled' if purchase_job == status
    css
  end

end
