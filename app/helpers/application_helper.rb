module ApplicationHelper

  def recent_requests
    Request.recent.approved.limit(3)
  end
end
