class ExtraJob < ActiveRecord::Base
  attr_accessible :description, :job_id, :price
  belongs_to :job

  # validates_presence_of :description, :price
  validates_numericality_of :price

  scope :recent, order('id asc')
end
