class Job < ActiveRecord::Base
  attr_accessible :description, :title, :user_id, :price, :category_id, :location_id, :days
  attr_accessible :tag_list, :images_attributes, :extra_jobs_attributes

  belongs_to :user
  belongs_to :category
  belongs_to :location

  has_many :images, dependent: :destroy
  has_many :extra_jobs, dependent: :destroy
  has_many :purchase_jobs, dependent: :destroy

  validates_presence_of :user, :description, :title, :price, :category, :days
  validates_numericality_of :price, :days

  accepts_nested_attributes_for :images, :reject_if => :all_blank
  accepts_nested_attributes_for :extra_jobs, :reject_if => proc { |a| a[:description].blank? || a[:price].blank? }
  
  acts_as_taggable

  scope :recent, order('created_at desc')

  scope :find_by_title_or_description, lambda {|word| where("title ilike ? OR description ilike ?", "%#{word}%", "%#{word}%")}

  self.per_page = 10


  def full_title
    "I will #{title} for #{ActionController::Base.helpers.number_to_currency(price)}"
  end

  class << self

    def get_from_params (params)

      ### MAYBE CAN CHANGE THIS LOGIC TO A PRESENTER
      if params.key?(:category) 
        key = :category 
        filter_title = "All #{params[key]} Jobs"
        jobs = find_jobs_from_class(key, params[key])
      elsif params.key?(:location) 
        key = :location
        filter_title = "All Jobs in #{params[key]}"
        jobs = find_jobs_from_class(key, params[key])
      elsif params.key?(:user_id)
        user = User.includes(:jobs).find(params[:user_id])
        jobs = user.jobs
        filter_title = "All #{user.name} Jobs"
      elsif params.key?(:tag)
        jobs = Job.includes(:category).tagged_with(params[:tag])
        filter_title = "All Jobs with tag: #{params[:tag]} "
      elsif params.key?(:q)
        jobs = find_by_title_or_description(params[:q])
        filter_title = "Jobs"
      else
        jobs = includes(:category)
        filter_title = "All Posted Jobs"
      end
      [jobs, filter_title]
    end

    def find_jobs_from_class(key, value)
      klass = key.to_s.capitalize.constantize
      klass.includes(:jobs).find_by_name(value).jobs
    end

  end

end
