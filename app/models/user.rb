class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :name, :paypal_email, :description, :avatar
  # attr_accessible :title, :body
  has_many :jobs, dependent: :destroy
  has_many :requests, dependent: :destroy
  has_many :purchase_jobs, dependent: :destroy
  has_one :wallet, dependent: :destroy

  validates_uniqueness_of :name
  validates_presence_of :name

  # validates :email, presence: true, uniqueness: true, format: {with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i}
  validates :paypal_email, format: {with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i}, allow_blank: true

  mount_uploader :avatar, AvatarUploader

  acts_as_messageable

  before_create :build_default_wallet

  def to_s
    name
  end

  def mailboxer_email(object)
    email
  end

  ['active', 'finished', 'unreviewed'].each do |job_status|
    define_method "#{job_status}_jobs" do
      PurchaseJob.joins(job: :user).send(job_status).where('users.id' => self.id)  
    end
  end

  def self.find_by_name_like(word)
    where("name ilike ?", "%#{word}%")
  end

  private
  def build_default_wallet
    build_wallet
    true
  end
end
