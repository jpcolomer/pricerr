class Request < ActiveRecord::Base
  attr_accessible :category_id, :description, :user_id, :approved

  belongs_to :user
  belongs_to :category


  validates :description, presence: true
  scope :approved, where(approved: true)
  scope :non_approved, where(approved: false)
  scope :pending_approval, where(approved: nil)
  scope :recent, order('created_at DESC')

end
