class Category < ActiveRecord::Base
  attr_accessible :name

  has_many :jobs
  validates_presence_of :name
  validates_uniqueness_of :name

  def to_s
    name
  end
  
end
