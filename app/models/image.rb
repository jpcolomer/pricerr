class Image < ActiveRecord::Base
  attr_accessible :job_id, :image
  belongs_to :job

  validates_presence_of :image
  mount_uploader :image, ImageUploader

  scope :recent, order('id asc')
end
