class Wallet < ActiveRecord::Base
  attr_accessible :balance, :user_id
  validates_presence_of :user_id
  validates_uniqueness_of :user_id

  validates :balance, numericality: {:greater_than_or_equal_to => 0}

  belongs_to :user


end
