class PurchaseJob < ActiveRecord::Base
  attr_accessible :active, :job_id, :review, :user_id
  belongs_to :job
  belongs_to :user

  validates :job, presence: true
  validates :user, presence: true

  scope :active, where(active: true)
  scope :unreviewed, where(review: nil).where(active: false)
  scope :finished, where(active: false).where('review is not null')

  delegate :title, :days, :price, to: :job

  alias :buyer :user

  def deliver_date
    created_at.to_date + days
  end

end
