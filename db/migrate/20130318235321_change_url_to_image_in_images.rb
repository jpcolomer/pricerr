class ChangeUrlToImageInImages < ActiveRecord::Migration
  def up
    rename_column :images, :url, :image
  end

  def down
    rename_column :images, :image, :url
  end
end
