class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :url
      t.integer :job_id

      t.timestamps
    end
    add_index :images, :job_id
  end
end
