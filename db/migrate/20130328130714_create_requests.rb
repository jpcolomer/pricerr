class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.string :description
      t.integer :user_id
      t.integer :category_id
      t.boolean :approved

      t.timestamps
    end
    add_index :requests, :user_id
    add_index :requests, :category_id
    add_index :requests, :approved
  end
end
