class AddPaypalEmailAndDescriptionAndImageToUsers < ActiveRecord::Migration
  def change
    add_column :users, :paypal_email, :string
    add_column :users, :description, :text
    add_column :users, :avatar, :string
  end
end
