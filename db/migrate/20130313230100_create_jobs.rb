class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.integer :user_id
      t.text :description
      t.string :title

      t.timestamps
    end
  end
end
