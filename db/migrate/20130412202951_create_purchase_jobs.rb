class CreatePurchaseJobs < ActiveRecord::Migration
  def change
    create_table :purchase_jobs do |t|
      t.boolean :active
      t.text :review
      t.integer :user_id
      t.integer :job_id

      t.timestamps
    end
    add_index :purchase_jobs, [:user_id, :job_id]
  end
end
