class AddDaysToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :days, :integer
  end
end
