class CreateExtraJobs < ActiveRecord::Migration
  def change
    create_table :extra_jobs do |t|
      t.string :description
      t.integer :price
      t.integer :job_id

      t.timestamps
    end
    add_index :extra_jobs, :job_id
  end
end
