class CreateWallets < ActiveRecord::Migration
  def change
    create_table :wallets do |t|
      t.integer :user_id
      t.decimal :balance, default: 0

      t.timestamps
    end
    add_index :wallets, :user_id, unique: true
    add_foreign_key :wallets, :users
  end
end
