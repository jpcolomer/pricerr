# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# user = User.create(name: 'jp', email: 'jpcolomer@gmail.com', password: '12345678')
# job = user.jobs.create(title: 'I will code your app', price: 100, description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vestibulum porttitor dui et dapibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam erat volutpat. Donec euismod sapien et nisi viverra venenatis. Proin at lorem justo, non metus.')

categories_array = ['Accounting', 'Digital Products', 'Events', 'Facebook', 'House Chores', 'Learning', 'Marketing', 'Other', 'Programming', 'Services', 'Shopping', 'Skilled', 'Travel']
categories = Category.create( categories_array.map { |category| {name: category} } )

locations_array = ['Alabama', 'Arizona', 'California', 'Florida', 'Illinois', 'Nevada', 'New York', 'Ohio', 'Texas', 'Utah', 'Vermont']
locations = Location.create( locations_array.map{|location| {name: location}} )


# 200.times do 
#   job = user.jobs.new(title: '...', price: rand(10..30), days: rand(5..40), description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vestibulum porttitor dui et dapibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam erat volutpat. Donec euismod sapien et nisi viverra venenatis. Proin at lorem justo, non metus.')
#   job.category = categories.sample
#   job.location = locations.sample
#   job.tag_list = ['programming', 'android', 'ror', 'ruby', 'cloth'].sample
#   job.save
# end


# admin = Admin.create(email: 'admin@pricerr.com', password: '12345678')

# 20.times do
#   request = Request.create(description: 'Lorem ipsum Officia dolore nostrud eiusmod elit officia', user_id: user.id, category_id: categories.sample.id, approved: [false, true, nil].sample)
# end


# user2 = User.create(name: 'example', email: 'example@example.com', password: '12345678')

# user2 = User.last
# 10.times {Job.all.sample.purchase_jobs.create(user_id: user2.id, active: true)}

# 3.times do |i|
#   user.send_message(user2, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod', "Lorem ipsum #{i}")
# end
# job.category = Category.find_by_name('Programming')

# User.all.each do |user|
#   user.create_wallet
# end